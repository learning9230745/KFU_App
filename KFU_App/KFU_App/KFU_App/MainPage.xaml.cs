﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace KFU_App
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        // Обработчик события нажатия кнопки "Расчитать КФУ"
        private async void CalculateKFU_Clicked(object sender, EventArgs e)
        {
            if (double.TryParse(SK.Text, out double numSK) && double.TryParse(DZ.Text, out double numDZ) && double.TryParse(PASS.Text, out double numPASS))
            {
                double KFU = (numSK + numDZ) / numPASS;

                if (KFU >= 0.8 && KFU <= 1.1)
                {
                    await DisplayAlert("Результат", $"Значение KFU является оптимальным: {KFU}", "OK");

                }
                else
                {
                    await DisplayAlert("Результат", $"Значение KFU : {KFU}", "OK");
                }


            }
        }

    }
}
